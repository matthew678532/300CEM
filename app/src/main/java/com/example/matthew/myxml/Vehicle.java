package com.example.matthew.myxml;

/**
 * Created by Matthew on 13/10/2016.
 */

public class Vehicle {
    public static int counter = 0;
    private String make;
    private int year;
    private String message;
    private int price;
    private double engine;

    public Vehicle() {
        this.make = "Volvo";
        this.year = 2012;
        this.message = "This is the default message.";
    }

    public Vehicle(String make, int year, int price, double engine) {
        this.make = make;
        this.year = year;
        this.price = price;
        this.engine = engine;
        this.message = "Your car is a " + make + " built in " + year + ".";
        count();
    }

    public int getPrice() {
        return price;
    }

    public double getEngine() {
        return engine;
    }

    public Vehicle(String make) {
        this();
        this.make = make;
        message = "You didn't type in year value.";
        count();
    }

    public String getMake() {
        return make;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }

    private void count(){
        this.counter++;
    }

    interface Controllable {
        void control();
    }
}

