package com.example.matthew.myxml;

/**
 * Created by Matthew on 13/10/2016.
 */

class Diesel extends Vehicle implements Vehicle.Controllable {

    private String type;

    public Diesel(String make, int year, int price, double engine) {
        super(make, year, price, engine);
        this.type = "Diesel";
    }

    @Override
    public void control() {
        setMessage(super.getMessage() + " Emission is under control.");
    }

    @Override
    public String getMessage() {
        control();
        return super.getMessage();
    }
}
