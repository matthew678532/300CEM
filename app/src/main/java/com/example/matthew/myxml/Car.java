package com.example.matthew.myxml;

/**
 * Created by Matthew on 13/10/2016.
 */

class Car extends Vehicle {

    private String color;

    public Car(String make, int year, String color, int price, double engine) {
        super(make, year, price, engine);
        this.color = color;
        setMessage(getMessage() + " I like your shining " + color + " color.");
    }
}